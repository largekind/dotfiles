#!/bin/bash

DOT_FILES=( .profile .bashrc .vimrc)
ENV=home

for file in ${DOT_FILES[@]}
do
    if [ -a $HOME/$file ]; then
	echo "ファイルが存在しますから何もしないよ: $file"
    else
	ln -s $HOME/dotfiles/$ENV/$file $HOME/$file
	echo "シンボリックリンクを貼りました: $file"
    fi
done
