# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
# set to avoid garbled character when displaying japanese by lark
case "$TERM" in
    linux)
	export LANG="C"
	export LANGUAGE="C"
	export LC_MESSAGES="C"
	export LC_CTYPE="C"
	export LC_COLLATE="C";;
    *)
	export LANG="ja_JP.UTF-8"
	export LANGUAGE="ja:en_GB:en"
	export LC_MESSAGES="ja_JP.UTF-8"
	export LC_CTYPE="ja_JP.UTF-8"
	export LC_COLLATE="ja_JP.UTF-8";;
esac


