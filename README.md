# my dot file
My using configlation files
## usage

ただの自分用のdotfile  
inst.shでシンボリックリンクが貼れるが、emacs.d/init.elは自分の力では無理だったんで、以下のコマンドでなんとかしてください  
ln -s init.el ~/.emacs.d/init.el  
あるいはコピーして対策するのもok ちゃんとした方法があるかもしれないが、怠いのでいつの日か
と思ってたけど当分無いので保留。最近Vimしか使ってないし

## vimrc

以下のコマンドを実行してvundleを持っておくこと

```bash
git clone http://github.com/gmarik/vundle.git ~/.vim/bundle/Vundle.vim
```

その後vim上でパッケージをインストールする。

```vim
:PluginInstall
```
