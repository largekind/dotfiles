;;
;; .emacs for 2014-H26LR
;; 2015-5-14

;; Japanese environment
;;　
;(require 'un-define)
(coding-system-put 'utf-8 'category 'utf-8)
(set-language-info "Japanese" 'coding-priority (cons 'utf-8 (get-language-info "Japanese" 'coding-priority)))
(set-language-environment "Japanese")
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8) 

; システム全体でutf-8を優先的に利用する
(prefer-coding-system 'utf-8)
; デフォルトコーティングをutf-8にする by larksiper 2015 12/6
(set-default-coding-systems 'utf-8)
; prefer-coding-system を utf-8 にするとディレクトリ名の
; 非ASCIIはすべてutf-8として認識する。
; USBメモリの日本語ファイル名やフォルダには対応できないかも。
; by tarai 2015-5-14


; .texファイルを読み込むとyatex処理系が動いて文字コードを変えてしまうため、
; .emacsでyatex関係の変数を設定する。
; 標準で作成するファイルをUTF-8エンコードにする（はず）
; ptexlive環境などで標準とする。
(setq default-buffer-file-coding-system 'utf-8)
(setq default-file-coding-system 'utf-8)
(setq YaTeX-kanji-code 4)
(setq latex-message-kanji-code 4)

; yatex-modeでの自動改行抑制
(add-hook 'yatex-mode-hook'(lambda ()(setq auto-fill-function nil)))

; デフォルトではyatexではなくauctexが上がるので、
; yatexを上げる
(setq auto-mode-alist
           (cons (cons "\\.tex$" 'yatex-mode) auto-mode-alist))
(autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)

;;; 起動するブラウザを Firefox に設定
(setq browse-url-netscape-program "/usr/bin/firefox")

;;; anthy-el 
;(require 'anthy)
;(setq default-input-method "japanese-anthy")

;;; skkを使いたい人はskkにするとよい
; (set-default-input-method "japanese-skk")

;;; mozc は挙動がおかしいのでおすすめしません
; (require 'mozc)
; (setq default-input-method "japanese-mozc")
; 変換候補を下部echo-areaに表示する
; (setq mozc-candidate-style 'echo-area)

;;; インデント調整なし
(setq-default indent-tabs-mode nil)

; 色つけあり
(global-font-lock-mode t)

; 起動画面抑制あり
(setq inhibit-startup-message t)

;
; emacs Options がセーブする内容が以下に書かれる
;
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
)

(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 143 :width normal :foundry "unknown" :family "VL ゴシック")))))

;; ぱとば　パとバとヴァ ぺとべ ペとベ
;; 
(setq x-select-enable-clipboard t)

; エンコーディングをutf-8にする。＠yatex
(setq YaTeX-kanji-code 4)
;yatex　C-c C-t jにおける自動的にコンパイルする際dvipdfmxを行う
(setq tex-command "sh ~/.platex2pdf")

;el-getの設定
(when load-file-name
  (setq user-emacs-directory (file-name-directory load-file-name)))

(add-to-list 'load-path (locate-user-emacs-file "el-get/el-get"))
(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))
;;
;;el-getで取得するパッケージ関連
;;

;; magit
(el-get-bundle magit)
(global-set-key (kbd "C-x g") 'magit-status)


;; Auto Complete
(el-get-bundle auto-complete)
(require 'auto-complete-config)
(ac-config-default)
(add-to-list 'ac-modes 'text-mode)         ;; text-modeでも自動的に有効にする
(add-to-list 'ac-modes 'fundamental-mode)  ;; fundamental-mode
(add-to-list 'ac-modes 'org-mode)
(add-to-list 'ac-modes 'yatex-mode)
(ac-set-trigger-key "TAB")
(setq ac-use-menu-map t)       ;; 補完メニュー表示時にC-n/C-pで補完候補選択

;; ruby-mode
(el-get-bundle ruby-mode)
 (autoload 'ruby-mode "ruby-mode"
   "Mode for editing ruby source files" t)
  (add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Capfile$" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))
  (add-to-list 'auto-mode-alist '("[Rr]akefile$" . ruby-mode))

;; ruby-block
(el-get-bundle ruby-block)
(require 'ruby-block)
(ruby-block-mode t)
(setq ruby-block-highlight-toggle t)

;; robe
(el-get-bundle robe)
(autoload 'robe-mode "robe" "Code navigation, documentation lookup and completion for Ruby" t nil)
(autoload 'robe-ac-setup "robe-ac" "robe auto-complete" nil nil)
(add-hook 'robe-mode-hook 'robe-ac-setup)

;; tree-undo
 (el-get-bundle undo-tree)
 (global-undo-tree-mode t) ; デフォルトをundo-treeのundoにする
;; C-M-z でredoを行えるようにする
 (define-key global-map (kbd "C-M-z") 'undo-tree-redo)

;; markdown-mode
;; TODO M-n M-p が衝突していると思うので削除する
(el-get-bundle markdown-mode)
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
